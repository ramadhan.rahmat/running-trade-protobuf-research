export default {
  websocket: {
    fileName: "securities/transactional/datafeed/v1/websocket.proto",
    path: {
      websocketRequest: "securities.transactional.datafeed.v1.WebsocketRequest",
      websocketChannel: "securities.transactional.datafeed.v1.WebsocketChannel",
      websocketWrapMessageChannel:
        "securities.transactional.datafeed.v1.WebsocketWrapMessageChannel",
      runningTrade: "securities.transactional.datafeed.v1.RunningTrade",
    },
  },
};
