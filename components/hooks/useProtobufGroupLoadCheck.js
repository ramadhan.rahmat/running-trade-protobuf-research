import { Type } from "protobufjs";
import { useEffect, useState } from "react";

/**
 *
 * @param {Type[]} groups
 */
const useProtobufGroupLoadCheck = (groups) => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (!groups.includes(undefined)) setLoading(false);
  }, [...groups]);

  return loading;
};

export default useProtobufGroupLoadCheck;
