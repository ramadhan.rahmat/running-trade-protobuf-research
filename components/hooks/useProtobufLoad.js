import { useEffect, useRef, useState } from "react";
import { Type } from "protobufjs";
import protoFilePath from "../constants/protoFilePath";
import getProtobufInstance from "../utils/getProtobufInstance";

/**
 *
 * @param {*} filename
 * @param {*} path
 * @returns {Type} protoRoot
 */
const useProtobufLoad = (filename, path) => {
  const [type, setType] = useState();
  const initLoad = useRef(false);

  useEffect(() => {
    if (initLoad.current) return;
    initLoad.current = true;

    const protobufInstance = getProtobufInstance();
    protobufInstance.load(filename, function (err, root) {
      console.log({ root });
      if (err) throw err;
      setType(root.lookupType(path));
    });
  }, []);

  return type;
};

export default useProtobufLoad;
