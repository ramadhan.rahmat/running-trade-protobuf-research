import protobuf from "protobufjs/light";

/**
 *
 * @returns
 */
const getProtobufInstance = () => {
  const instance = new protobuf.Root();
  instance.resolvePath = (origin, target) => {
    return "protos/" + target;
  };

  return instance;
};

export default getProtobufInstance;
